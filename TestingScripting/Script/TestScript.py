﻿'''
def TestScript():
  p = TestedApps.Items[0].Run()
  if not p.Exists:
    Log.Message("Application was not launched")#
'''
'''
    #Runs the "JD_Update" tested application.
  p = TestedApps.JD_Update.Run(1, True)
  if not p.Exists:
    Log.Message("Application was not launched")
'''

import shutil
import subprocess
import os
from os.path import exists

def Run_Command(cmd):
  return subprocess.run(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).stdout.decode(errors="ignore")


#-------------------------------------Prerequisites----------------------------------------#

def TopTier_4_2_1_Downloads_Scheduled():
#-------------------------------------Test_Procedures--------------------------------------#
#STEP 1
  dir = r"F:\JTDI\download_files\data"
  folder = os.listdir(dir)
  for f in folder:
    os.remove(os.path.join(dir, f))
#Expected Result
  if len(folder) > 0:
    Log.Message("{} is not empty".format(dir))
  
#STEP 2
  src=r'C:\jenkins\LITMIS-NavAir-Test-Config\Test_Data\4_8_test.txt'
  shutil.copy(src, dir)
#Expected Result
  if not exists(src):
    Log.Message("4_8_test.txt is not in {}".format(dir))
    
#STEP 3 
  #subprocess.run("cd C:\Program Files\Google\Chrome\Application\\ & chrome.exe", shell=True)
  #Run_Command("E: & cd Program Files\jdms\jdupdate\backend\\ & JDUpdate.exe startjob --id=3")
  Run_Command("cd C:\Program Files\Google\Chrome\Application\\ & chrome.exe")
  
  
    
